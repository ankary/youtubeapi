import axios from "axios";

// const KEY = "AIzaSyAkPBILcLPQotjxeL_qUMtoiGWlROeX7KU";
const KEY = "AIzaSyBcqDj374vfPW_lOwwFVSB-nrPSCgA5wqE";

export const baseParams = {
    part: "snippet",
    maxResults: 5,
    key: KEY
};

export default axios.create({
    baseURL: "https://www.googleapis.com/youtube/v3",
    params: baseParams
});
