import React from "react";
import SearchBar from "./SearchBar";
import youtube, { baseParams } from "../apis/youtube";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";
import "./../App.css";

class App extends React.Component {
  state = { videos: [], selectedVideo: null };

  componentDidMount() {
    this.onTermSubmit("apex");
  }

  onTermSubmit = async term => {
    try {
      const response = await youtube.get("/search", {
        params: {
          ...baseParams,
          q: term
        }
      });

      this.setState(
        {
          videos: response.data.items
        },
        () => {
          if (
            document.querySelector("#toggle-button").innerHTML === "stop playing"
          ) {
            this.setState({ selectedVideo: response.data.items[0] });
          }
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  onVideoSelect = video => {
    this.setState({ selectedVideo: video });
  };

  onButtonClick = event => {
    if (event.target.innerHTML === "stop playing") {
      event.target.innerHTML = "keep playing";
      document.getElementById("toggle-button").classList.add("keep");
      document.getElementById("toggle-button").classList.remove("stop");
    } else {
      event.target.innerHTML = "stop playing";
      document.getElementById("toggle-button").classList.add("stop");
      document.getElementById("toggle-button").classList.remove("keep");
    }
  };

  render() {
    return (
      <div className="ui container">
        <SearchBar onFormSubmit={this.onTermSubmit} />
        <div className="feature">
          <div>
            Keep/Stop videos from playing while searching by toggling the below
            button!
          </div>
          <div>
            <div>At the moment videos will </div>
            <button
              id="toggle-button"
              onClick={this.onButtonClick}
              className="toggle-btn stop"
            >
              stop playing
            </button>
          </div>
        </div>
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetail video={this.state.selectedVideo} />
            </div>
            <div className="five wide column">
              <VideoList
                onVideoSelect={this.onVideoSelect}
                videos={this.state.videos}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
